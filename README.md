### Server:
Digital ocean (ibroox - 104.131.89.192)
url: ououi.ibroox.com
appuser: ououi_user
appuser password: [default]
mysql user: root
password: [default]
port 3005

### Stack:
- node 0.12.2 (no nvm)
- bower
- ReactJS
- PM2 (https://github.com/Unitech/pm2) [process manager]
- nginx
- webpack (npm prestart)

#Repo
- bitbucket: "ououi" git@bitbucket.org:broox9/ououi.git
