var React = require('react');
var MapPanel = require('./components/map-panel.jsx');
var InfoPanel = require('./components/info-panel.jsx');
var Compass = require('./components/compass.jsx');
//var gmaps_api_key = 'AIzaSyBxPzhDXUPPcDaVwy34HIvwzWTdKwCEPBY';

var Ououi = React.createClass({
  getInitialState: function() {
    return {
      coords: {
        latitude: 40.7127,
        longitude: -74.0059
      },
      heading: null,
      latLng: null, //new google.maps.LatLng(40.7127,-74.0059),
      address_info: null
    };
  },

  componentWillMount: function () {
    this.tryCurrentPosition();
  },

  componentDidMount: function () {
    this.reverseGeo();
    this.setCompassListeners();
  },

  tryCurrentPosition: function () {
    var self = this;
    console.log("calling for current position");
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    navigator.geolocation.getCurrentPosition(self.processCurrentPosition, function (error) {console.log(error); alert(error.message)}, options )
  },

  processCurrentPosition: function (loc) {
    var self = this;
    var latLng = new google.maps.LatLng(loc.coords.latitude,loc.coords.longitude);
    console.log(loc, latLng);
    this.setState({
      latLng: latLng,
      coords: {
        latitude: loc.coords.latitude,
        longitude: loc.coords.longitude
      }
    }, self.reverseGeo);
  },

  refreshLocation: function () {
    this.tryCurrentPosition();
  },

  reverseGeo: function () {
    new google.maps.Geocoder().geocode({latLng: this.state.latLng}, this.parseReverseGeoData)
  },

  parseReverseGeoData: function (results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        if (results.length <= 6) {
          this.formatData('standard', 'locality', results[0].address_components)
        }
        else if (results.length > 6) {
          this.formatData('advanced','neighborhood',results[0].address_components)
        }
        else {console.log("ELSE", results.length) }
      }
    } else { }
  },

  formatData: function (type, main, results) {
    var addr_obj = {type: type, main: main};
    for (var i = 0, n = results.length; i < n; i++) {
      addr_obj[results[i]['types'][0]] = results[i].short_name;
    }
    this.setState({address_info: addr_obj});
  },

  setCompassListeners: function () {
    var self = this;
    //window.addEventListener('devicemotion', self.updateDeviceMotion, false);
    window.addEventListener('deviceorientation', self.updateDeviceOrientation, false);
  },

  updateDeviceMotion: function (e) {
    console.log("DeviceMotion", e);
  },

  updateDeviceOrientation: function (e) {
    console.log("DeviceOrientation", e);

    var heading =  360 - (e.alpha || 0).toFixed(0);

    if ('webkitCompassHeading' in e) {
      heading = -(360 - e.webkitCompassHeading.toFixed(0));
    }
    this.setState({heading: heading})
  },

  render: function () {
    return (
      <div id="app-wrapper">
        <section id="panels">
          <MapPanel latLng={this.state.latLng}  />
          <InfoPanel latLng={this.state.latLng} coords={this.state.coords} refreshLocation={this.refreshLocation} address_info={this.state.address_info}/>
        </section>
        <Compass heading={this.state.heading} />
      </div>
    )
  }
});


React.initializeTouchEvents(true);
React.render(<Ououi />, document.querySelector('#app-container'));