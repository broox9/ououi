var React = require('react');

var RefreshButton = React.createClass({
  refreshLocation: function () {
    console.log("passing refresh, infopanel");
    this.props.refreshLocation();
  },

  render: function () {
    return <a id="refresh-button" className="fa fa-repeat" onTouchEnd={this.refreshLocation} onClick={this.refreshLocation}></a>
  }
});

module.exports = RefreshButton;