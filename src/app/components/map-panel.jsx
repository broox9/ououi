var React = require('react');
var Map;



var MapPanel = React.createClass({
  shouldComponentUpdate: function (nextProps) {
    if (this.props.latLng)  {
      console.log("UPDATING MAP")
      return !(this.props.latLng.A == nextProps.latLng.A && this.props.latLng.F === nextProps.latLng.F)
    }
    return false;
  },

  componentWillReceiveProps: function (newProps) {
    console.log('new map props', arguments, Map);
    if (Map == null) {return}
  },


  componentDidMount: function () {
     var mapOpts = {
       zoom: 19
     };

    Map = new google.maps.Map(document.querySelector('#map-canvas'), mapOpts);
    this.setMapCenter(this.props.latLng);
  },

  componentDidUpdate: function (prevProps) {
    this.setMapCenter(this.props.latLng);
  },

  setMapCenter: function (latLng) {
    console.log("SETTING CENTER", latLng)
    Map.setCenter(latLng);

    //console.log("Icon", 'data:image/svg+xml;base64,'+ btoa(marker_svg))
    var marker = new google.maps.Marker({
      map: Map,
      position: Map.getCenter(),
      animation: google.maps.Animation.DROP,
      draggable: false
    });


  },

  render: function () {
    return (
      <div id="map-panel">
        <div id="map-canvas"></div>
      </div>
    )
  }
});


module.exports = MapPanel;