var React = require("react");

var AddressInfo = React.createClass({
  shouldComponentUpdate: function (nextProps) {
//    if (!!(this.props.latLng && this.props.latLng.A && this.props.latLng.F))  {
//
//      return (this.props.latLng.A !== nextProps.latLng.A || this.props.latLng.F !== nextProps.latLng.F)
//    }
    return true;
  },

  render: function () {
    if (!this.props.address_info) {
      return (<h3 className="no-address-info">No Address Info Presently</h3>)
    }
    else if (this.props.address_info.type == 'standard') {
      return (
        <ul id="address-info-box">
          <li className="main">
            <span className="main-info">{this.props.address_info.main}</span>
            {this.props.address_info[this.props.address_info.main]}
          </li>
          <li>{this.props.address_info.street_number} {this.props.address_info.route}</li>
          <li>{this.props.address_info.administrative_area_level_2}</li>
          <li>
            {this.props.address_info.administrative_area_level_1}, {this.props.address_info.postal_code}
          </li>
        </ul>
      )
    }
    else if (this.props.address_info.type == 'advanced') {
      return (
        <ul id="address-info-box">
          <li className="main">
            <span className="main-info">{this.props.address_info.main}</span>
          {this.props.address_info[this.props.address_info.main]}
          </li>
          <li>{this.props.address_info.street_number} {this.props.address_info.route}</li>
          <li>{(this.props.address_info.sublocality_level_1) ||  this.props.address_info.locality}</li>
          <li>{this.props.address_info.administrative_area_level_2}</li>
          <li>
          {this.props.address_info.administrative_area_level_1}, {this.props.address_info.postal_code}
          {(this.props.address_info.postal_code_suffix)? "-" + this.props.address_info.postal_code_suffix : ""}
          </li>
        </ul>
      )
    }

  }
});


var StandardAddress = {}


module.exports = AddressInfo;