var React = require('react');
var RefreshButton = require('./refresh-button.jsx');
var AddressInfo = require('./address-info.jsx');

var InfoPanel = React.createClass({
//  shouldComponentUpdate: function (newProps, newState) {
//    console.log("INFO PANEL NEW PROPS", newProps.latLng)
//    if (!newProps.latLng) {return false}
//    return (newProps.latLng.A == this.props.latLng.A && newProps.latLng.F == this.props.latLng.F);
//  },

  reset: function () {
    if(!confirm("Restart App?")) {
      return false;
    }
    window.location.reload();
  },

  render: function () {
    return <div id="info-panel">
      <AddressInfo address_info={this.props.address_info} />
      <RefreshButton  refreshLocation={this.props.refreshLocation} />
      <a id="reset-button" className="fa fa-power-off" onTouchEnd={this.reset} onClick={this.reset}></a>
    </div>
  }
});

module.exports = InfoPanel;