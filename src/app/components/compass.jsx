var React = require('react');

var Compass = React.createClass({
  componentWillReceiveProps: function (newProps) {
    this.spin(newProps.heading);
  },

  spin: function (heading) {
    var svg = React.findDOMNode(this.refs.arrow);

    console.log("SPIN", heading);
    svg.style.transform = "rotate(" + heading + "deg)";
    svg.style.webkitTransform = "rotate(" + heading + "deg)";
  },

  render: function () {
    return (
      <svg id="marker-compass" viewBox="0 0 200 200">
        <circle id="marker-circle" cx="100" cy="100" r="95" />
        <polygon id="marker-arrow" ref="arrow" points="100,50 50,150 100,130 150,150" />
      </svg>
    )
  }

});

module.exports = Compass;