var path = require('path');

module.exports = {

  entry: path.resolve(__dirname, '../src/app/app.js'),
  output: {
    path: './public/js',
    filename: 'bundle.js'
  },

  module: {
    loaders: [
      {
        //test: /src\/.+.js$/,
        exclude: /node_modules|lib/,
        loader: 'babel'
      }
    ]
  }
};
