var http = require('http');
var fs = require('fs');

// Send 404
function notFound (res) {
  res.writeHead(404, {"Content-Type": "text/plain"});
  res.write('404, Page Not Found');
  res.end();
}

var server = http.createServer(function (req, res) {
  if (req.method === "GET" && req.url ="/") {
    res.writeHead(200, {"Content-Type": "text/html"});
    fs.createReadStream('../public/index.html').pipe(res);
  } else {
    notFound(res);
  }

});

server.listen(3005);
