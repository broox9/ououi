var static = require('node-static');
var http = require('http');

var public_dir = new static.Server('./public', {cache: 30}); //cached for 30 seconds

//Easy Peasy with Node Static
http.createServer(function (req, res) {
  req.addListener('end', function () {
    public_dir.serve(req,res);
  }).resume()
}).listen(3005);
